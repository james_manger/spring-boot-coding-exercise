package com.telstra.codechallenge.repos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 *Git repo metadata for a collections of repos.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class RepoList {

  private Repo[] items;

  public Repo[] getItems() {
    return items;
  }
}
