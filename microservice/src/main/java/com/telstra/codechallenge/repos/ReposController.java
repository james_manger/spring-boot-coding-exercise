package com.telstra.codechallenge.repos;

import java.util.Arrays;
import java.util.List;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Microservice REST API for interacting with a site that hosts repos.
 */
@RestController
public class ReposController {

  private ReposService reposService;

  public ReposController(
      ReposService reposService) {
    this.reposService = reposService;
  }

  /**
   * Returns an array with metadata on the most starred repos
   * in the last week.
   * @param n  the maximum number of repos to return
   */
  @RequestMapping(path = "/repos/hot", method = RequestMethod.GET)
  public List<Repo> hotRepos(@RequestParam(value="n", defaultValue="10") int n) {
    return Arrays.asList(reposService.getHotRepos(n));
  }

}
