package com.telstra.codechallenge.repos;

import java.util.Arrays;
import java.time.Instant;
import java.time.Duration;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Manage interactions with a site that hosts repos.
 */
@Service
public class ReposService {

  /** Maximum number of repos that will be returned. */
  public static final int MAX_REPOS = 100; // TODO: make it a configurable property

  private static final String SEARCH_PATH = "/search/repositories";

  private String reposBaseUrl;

  private Duration ttl; // time-to-live

  private RestTemplate restTemplate;

  private static class CachedRepos {
    Repo[] repos;
    Instant time;
    CachedRepos(Repo[] repos, Instant time)
    { this.repos = repos; this.time = time; }
  }

  private volatile CachedRepos cachedRepos;

  /**
   * Creates a service for interacting with a repo hosting site.
   * Caching is used so requests to the site can be rate-limited.
   * @param ttl  time in seconds between requests out to the site
   */
  public ReposService(
      RestTemplate restTemplate,
      @Value("${repos.base.url}") String reposBaseUrl,
      @Value("${repos.ttl}") long ttl)
  {
    this.restTemplate = restTemplate;
    this.ttl = Duration.ofSeconds(ttl);
    this.reposBaseUrl = reposBaseUrl;
  }

  /**
   * Returns an array of the highest starred repos in the last week.
   * Results are cached to avoid rate limits.
   *
   * @return  an array of up to n repos, in order of decreasing stars
   */
  public Repo[] getHotRepos(int n) {

    if (n < 1)
      throw new IllegalArgumentException("ask for at least 1 repo: " + n);

    CachedRepos cache = cachedRepos;
    if (cache == null || Instant.now().isAfter(cache.time.plus(ttl))) { // refresh
      String date = LocalDate.now().minusDays(7).toString(); // TODO: could consider timezones if we really need a 7x24hr period
      String url = reposBaseUrl + SEARCH_PATH + "?sort=stars&order=desc&q=" + date + "&per_pag=" + MAX_REPOS;
      RepoList rr = restTemplate.getForObject(url, RepoList.class);
      cache = new CachedRepos(rr.getItems(), Instant.now());
      cachedRepos = cache;
    }

    Repo[] repos = Arrays.copyOf(cache.repos, Integer.min(n, cache.repos.length));

    return repos;
  }

}
