package com.telstra.codechallenge.repos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Git repo metadata.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Repo {

  private String name;
  private String description;
  private String language;
  private String html_url;
  private long watchers_count;

}
