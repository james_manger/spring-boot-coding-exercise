# See
# https://github.com/intuit/karate#syntax-guide
# for how to write feature scenarios
Feature: As an api user I want to retrieve a list of the hottest repos

  Scenario: Get the 5 hottest repos
    Given url microserviceUrl
    And path '/repos/hot'
    And param n = 5
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    # Define the required schema
    * def repoSchema = { html_url : '#string', watchers_count : '#number', language : '##string', description : '#string', name : '#string' }
    # The response should have an array of 5 repo objects
    And match response == '#[5] repoSchema' 

  Scenario: Get the 10 hottest repos immediately (within rate limit)
    Given url microserviceUrl
    And path '/repos/hot'
    And param n = 10
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    # Define the required schema
    * def repoSchema = { html_url : '#string', watchers_count : '#number', language : '##string', description : '#string', name : '#string' }
    # The response should have an array of 5 repo objects
    And match response == '#[10] repoSchema' 

  Scenario: Asking for zero of a negative number of repos should fail
    Given url microserviceUrl
    And path '/repos/hot'
    And param n = -1
    When method GET
    Then status 500
